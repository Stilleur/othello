//Ajout

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/signalfd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netdb.h>
#include <signal.h>
#include <inttypes.h>

#include <gtk/gtk.h>


#define MAXDATASIZE 256

// Fin ajout


/* Variables globales */
int damier[8][8];	// tableau associe au damier
int couleur;		// 0 : pour noir, 1 : pour blanc

int port;		// numero port passe a l'appel

char *addr_j2, *port_j2;	// Info sur adversaire

// Ajout

pthread_t thr_id;	// Id du thread fils gerant connexion socket

int sockfd, newsockfd=-1; // descripteurs de socket
int addr_size;	 // taille adresse
struct sockaddr *their_addr;	// structure pour stocker adresse adversaire

fd_set master, read_fds, write_fds;	// ensembles de socket pour toutes les sockets actives avec select
int fdmax;			// utilise pour select

int status;
struct addrinfo hints, *servinfo, *p;

// Fin ajout  


/* Variables globales associées à l'interface graphique */
GtkBuilder  *  p_builder   = NULL;
GError      *  p_err       = NULL;



// Entetes des fonctions  

/* Fonction permettant de changer l'image d'une case du damier (indiqué par sa colonne et sa ligne) */
void change_img_case(int col, int lig, int couleur_j);

/* Fonction permettant changer nom joueur blanc dans cadre Score */
void set_label_J1(char *texte);

/* Fonction permettant de changer nom joueur noir dans cadre Score */
void set_label_J2(char *texte);

/* Fonction permettant de changer score joueur blanc dans cadre Score */
void set_score_J1(int score);

/* Fonction permettant de récupérer score joueur blanc dans cadre Score */
int get_score_J1(void);

/* Fonction permettant de changer score joueur noir dans cadre Score */
void set_score_J2(int score);

/* Fonction permettant de récupérer score joueur noir dans cadre Score */
int get_score_J2(void);

/* Fonction transformant coordonnees du damier graphique en indexes pour matrice du damier */
void coord_to_indexes(const gchar *coord, int *col, int *lig);

/* Fonction appelee lors du clique sur une case du damier */
static void coup_joueur(GtkWidget *p_case);

/* Fonction retournant texte du champs adresse du serveur de l'interface graphique */
char *lecture_addr_serveur(void);

/* Fonction retournant texte du champs port du serveur de l'interface graphique */
char *lecture_port_serveur(void);

/* Fonction retournant texte du champs login de l'interface graphique */
char *lecture_login(void);

/* Fonction retournant texte du champs adresse du cadre Joueurs de l'interface graphique */
char *lecture_addr_adversaire(void);

/* Fonction retournant texte du champs port du cadre Joueurs de l'interface graphique */
char *lecture_port_adversaire(void);

/* Fonction affichant boite de dialogue si partie gagnee */
void affiche_fenetre_gagne(void);

/* Fonction affichant boite de dialogue si partie perdue */
void affiche_fenetre_perdu(void);

/* Fonction appelee lors du clique du bouton Se connecter */
static void clique_connect_serveur(GtkWidget *b);

/* Fonction desactivant bouton demarrer partie */
void disable_button_statr(void);

/* Fonction appelee lors du clique du bouton Demarrer partie */
static void clique_connect_adversaire(GtkWidget *b);

/* Fonction desactivant les cases du damier */
void gele_damier(void);

/* Fonction activant les cases du damier */
void degele_damier(void);

/* Fonction permettant d'initialiser le plateau de jeu */
void init_interface_jeu(void);

/* Fonction reinitialisant la liste des joueurs sur l'interface graphique */
void reset_liste_joueurs(void);

/* Fonction permettant d'ajouter un joueur dans la liste des joueurs sur l'interface graphique */
void affich_joueur(char *login, char *adresse, char *port);

// XXX Network

typedef enum request_type {
	GAME_INITIATED,
	TOKEN_PLAYED,
	GAME_END
} request_type;

typedef struct requete {
	request_type type;
	int couleur;
	int col;
	int lig;
} requete;


void envoyer_requete(requete requete) {
	char buf[MAXDATASIZE];

	snprintf(buf, MAXDATASIZE, "%u,%u,%u,%u", htons(requete.type), htons(requete.couleur), htons(requete.col), htons(requete.lig));
	printf("Requete envoyee : type = %d, couleur = %d, colonne = %d, ligne = %d\n", requete.type, requete.couleur, requete.col, requete.lig);

	send(newsockfd, &buf, strlen(buf), 0);
}

requete recevoir_requete() {
	requete requete;
	char buf[MAXDATASIZE];
	char * saveptr;
	char * token;
	int temp;

	// Reception message requete client
	recv(newsockfd, buf, MAXDATASIZE, 0);

	// type
	token = strtok_r(buf, ",", &saveptr);
	sscanf(token, "%u", &temp);
	requete.type = ntohs(temp);

	// couleur
	token = strtok_r(saveptr, ",", &saveptr);
	sscanf(token, "%u", &temp);
	requete.couleur = ntohs(temp);

	// colonne
	token = strtok_r(saveptr, ",", &saveptr);
	sscanf(token, "%u", &temp);
	requete.col = ntohs(temp);

	// ligne
	token = strtok_r(saveptr, ",", &saveptr);
	sscanf(token, "%u", &temp);
	requete.lig = ntohs(temp);

	printf("Requete recu : type = %d, couleur = %d, colonne = %d, ligne = %d\n", requete.type, requete.couleur, requete.col, requete.lig);

	return requete;
}

// Ajout

/* Fonction transforme coordonnees du damier graphique en indexes pour matrice du damier */
void coord_to_indexes(const gchar *coord, int *col, int *lig)
{
	char *c;

	c=malloc(3*sizeof(char));

	c=strncpy(c, coord, 1);
	c[1]='\0';

	if(strcmp(c, "A")==0)
	{
		*col=0;
	}
	if(strcmp(c, "B")==0)
	{
		*col=1;
	}
	if(strcmp(c, "C")==0)
	{
		*col=2;
	}
	if(strcmp(c, "D")==0)
	{
		*col=3;
	}
	if(strcmp(c, "E")==0)
	{
		*col=4;
	}
	if(strcmp(c, "F")==0)
	{
		*col=5;
	}
	if(strcmp(c, "G")==0)
	{
		*col=6;
	}
	if(strcmp(c, "H")==0)
	{
		*col=7;
	}

	*lig=atoi(coord+1)-1;
}

/* Fonction transforme coordonnees du damier graphique en indexes pour matrice du damier */
void indexes_to_coord(int col, int lig, char *coord)
{
	char c;

	if(col==0)
	{
		c='A';
	}
	if(col==1)
	{
		c='B';
	}
	if(col==2)
	{
		c='C';
	}
	if(col==3)
	{
		c='D';
	}
	if(col==4)
	{
		c='E';
	}
	if(col==5)
	{
		c='F';
	}
	if(col==6)
	{
		c='G';
	}
	if(col==7)
	{
		c='H';
	}

	sprintf(coord, "%c%d\0", c, lig+1);
}

// Fin ajout


/* Fonction permettant de changer l'image d'une case du damier (indiqué par sa colonne et sa ligne) */
void change_img_case(int col, int lig, int couleur_j)
{
	char * coord;

	coord=malloc(3*sizeof(char));

	indexes_to_coord(col, lig, coord);

	if(couleur_j)
	{ // image pion blanc
		gtk_image_set_from_file(GTK_IMAGE(gtk_builder_get_object(p_builder, coord)), "UI_Glade/case_blanc.png");
	}
	else
	{ // image pion noir
		gtk_image_set_from_file(GTK_IMAGE(gtk_builder_get_object(p_builder, coord)), "UI_Glade/case_noir.png");
	}
}

/* Fonction permettant changer nom joueur blanc dans cadre Score */
void set_label_J1(char *texte)
{
	gtk_label_set_text(GTK_LABEL(gtk_builder_get_object (p_builder, "label_J1")), texte);
}

/* Fonction permettant de changer nom joueur noir dans cadre Score */
void set_label_J2(char *texte)
{
	gtk_label_set_text(GTK_LABEL(gtk_builder_get_object (p_builder, "label_J2")), texte);
}

/* Fonction permettant de changer score joueur blanc dans cadre Score */
void set_score_J1(int score)
{
	char *s;

	s=malloc(5*sizeof(char));
	sprintf(s, "%d\0", score);

	gtk_label_set_text(GTK_LABEL(gtk_builder_get_object (p_builder, "label_ScoreJ1")), s);
}

/* Fonction permettant de récupérer score joueur blanc dans cadre Score */
int get_score_J1(void)
{
	const gchar *c;

	c=gtk_label_get_text(GTK_LABEL(gtk_builder_get_object (p_builder, "label_ScoreJ1")));

	return atoi(c);
}

/* Fonction permettant de changer score joueur noir dans cadre Score */
void set_score_J2(int score)
{
	char *s;

	s=malloc(5*sizeof(char));
	sprintf(s, "%d\0", score);

	gtk_label_set_text(GTK_LABEL(gtk_builder_get_object (p_builder, "label_ScoreJ2")), s);
}

/* Fonction permettant de récupérer score joueur noir dans cadre Score */
int get_score_J2(void)
{
	const gchar *c;

	c=gtk_label_get_text(GTK_LABEL(gtk_builder_get_object (p_builder, "label_ScoreJ2")));

	return atoi(c);
}

// XXX Logic

/**
 * Détermine si le coup est valide
 */
int case_valide(int col, int lig) {
	return ((col >= 0) && (col < 8) && (lig >= 0) && (lig < 8));
}

/**
 * Modifie la couleur de la case sur le damier
 */
void set_case(int col, int lig, int couleur) {
	if (get_case(col, lig) != -2) {
		damier[col][lig] = couleur;
		change_img_case(col, lig, couleur);
	}
}

/**
 * Récupère la couleur de la case
 * Retourne -1 quand la case est vide
 * Retourne -2 en cas d'erreur
 */
int get_case(int col, int lig) {
	if (case_valide(col, lig)) {
		return damier[col][lig];
	}

	return -2;
}

/**
 * Permet de jouer un token
 * Renvoi 1 quand le coup est valide sinon renvoi 0
 */
int jouer_token(int col, int lig, int couleur, int dessiner) {
	int adversaire = abs(couleur - 1);
	int result = 0; // si le coup est correct
	int x, y, t; // x -> abscisse; y -> ordonnee;

	// Si la case est utilisée, renvoi 0
	int current_case = get_case(col, lig);
	if (current_case == 0 || current_case == 1) {
		return 0;
	}

	// droite
	x = col + 1;
	t = 0;
	while (case_valide(x, lig) && get_case(x, lig) == adversaire) {
		x++;
		t++;
	}

	if (case_valide(x, lig) && get_case(x, lig) == couleur && t > 0) {
		result = 1;
		// remplacer les cases entre {col, lig} et {x, lig} par "couleur"
		if(dessiner) {
			int i;
			for (i = 0; i <= t; i++) {
				set_case(col + i, lig, couleur);
			}
		}
	}

	// gauche
	x = col - 1;
	t = 0;
	while (case_valide(x, lig) && get_case(x, lig) == adversaire) {
		x--;
		t++;
	}

	if (case_valide(x, lig) && get_case(x, lig) == couleur && t > 0) {
		result = 1;
		// remplacer les cases entre {col, lig} et {x, lig} par "couleur"
		if(dessiner) {
			int i;
			for (i = 0; i <= t; i++) {
				set_case( col - i, lig, couleur);
			}
		}
	}

	// bas
	y = lig + 1;
	t = 0;
	while (case_valide(col, y) && get_case(col, y) == adversaire) {
		y++;
		t++;
	}

	if (case_valide(col, y) && get_case(col, y) == couleur && t > 0) {
		result = 1;
		// remplacer les cases entre {col, lig} et {col, y} par "couleur"
		if(dessiner) {
			int i;
			for (i = 0; i <= t; i++) {
				set_case(col, lig + i, couleur);
			}
		}
	}

	// haut
	y = lig - 1;
	t = 0;
	while (case_valide(col, y) && get_case(col, y) == adversaire) {
		y--;
		t++;
	}

	if (case_valide(col, y) && get_case(col, y) == couleur && t > 0) {
		result = 1;
		// remplacer les cases entre {col, lig} et {col, y} par "couleur"
		if(dessiner) {
			int i;
			for (i = 0; i <= t; i++) {
				set_case(col, lig - i, couleur);
			}
		}
	}


	// pente bas
	x = col + 1;
	y = lig + 1;
	t = 0;
	while (case_valide(x, y) && get_case(x, y) == adversaire) {
		x++;
		y++;
		t++;
	}

	if (case_valide(x, y) && get_case(x, y) == couleur && t > 0) {
		result = 1;
		// remplacer les cases entre {col, lig} et {x, y} par "couleur"
		if(dessiner) {
			int i;
			for (i = 0; i <= t; i++) {
				set_case(col + i, lig + i, couleur);
			}
		}
	}

	// pente haut
	x = col - 1;
	y = lig - 1;
	t = 0;
	while (case_valide(x, y) && get_case(x, y) == adversaire) {
		x--;
		y--;
		t++;
	}

	if (case_valide(x, y) && get_case(x, y) == couleur && t > 0) {
		result = 1;
		// remplacer les cases entre {col, lig} et {x, y} par "couleur"
		if(dessiner) {
			int i;
			for (i = 0; i <= t; i++) {
				set_case(col - i, lig - i, couleur);
			}
		}
	}

	//montee bas
	x = col - 1;
	y = lig + 1;
	t = 0;
	while (case_valide(x, y) && get_case(x, y) == adversaire) {
		x--;
		y++;
		t++;
	}

	if (case_valide(x, y) && get_case(x, y) == couleur && t > 0) {
		result = 1;
		// remplacer les cases entre {col, lig} et {x, y} par "couleur"
		if(dessiner) {
			int i;
			for (i = 0; i <= t; i++) {
				set_case(col - i, lig + i, couleur);
			}
		}
	}

	//montee haut
	x = col + 1;
	y = lig - 1;
	t = 0;
	while (case_valide(x, y) && get_case(x, y) == adversaire) {
		x++;
		y--;
		t++;
	}

	if (case_valide(x, y) && get_case(x, y) == couleur && t > 0) {
		result = 1;
		// remplacer les cases entre {col, lig} et {x, y} par "couleur"
		if(dessiner) {
			int i;
			for (i = 0; i <= t; i++) {
				set_case(col + i, lig - i, couleur);
			}
		}
	}


	return result;
}

void update_score() {
	int x, y, score, score_adversaire;

	score = 0;
	score_adversaire = 0;

	for (x = 0; x < 8; x++) {
		for (y = 0; y < 8; y++) {

			int couleur_case = get_case(x, y);

			if (couleur_case == couleur) {
				score += 1;
			} else if (couleur_case == abs(couleur - 1)) {
				score_adversaire += 1;
			}
		}
	}

	if (couleur == 0) {
		set_score_J1(score_adversaire);
		set_score_J2(score);
	} else {
		set_score_J1(score);
		set_score_J2(score_adversaire);
	}
}

int coup_possible() {
	int x, y;

	// parcours l'ensemble du damier
	for (x = 0; x < 8; x++) {
		for (y = 0; y < 8; y++) {
			// si la case est vide
			if(get_case(x, y) == -1) {
				// on essaye de jouer un token avec la couleur du joueur courant
				if(jouer_token(x, y, couleur, 0)) {
					// s'il peut jouer un token, alors un coup est possible
					return 1;
				}
			}
		}
	}

	return 0;
}

/**
 * Fonction appelee lors du clique sur une case du damier
 * Renvoi 1 si le coup est valide sinon renvoi 0
 */
static void coup_joueur(GtkWidget *p_case)
{
	int col, lig;

	// Traduction coordonnees damier en indexes matrice damier
	coord_to_indexes(gtk_buildable_get_name(GTK_BUILDABLE(gtk_bin_get_child(GTK_BIN(p_case)))), &col, &lig);

	// XXX Fonction coup_joueur
	// Le coup est-il valide ?
	if (jouer_token(col, lig, couleur, 1)) {
		//Geler le damier
		gele_damier();

		// Mettre à jour le score
		update_score();

		// Envoyer la requete
		requete requete;
		requete.type = TOKEN_PLAYED;
		requete.col = col;
		requete.lig = lig;
		requete.couleur = 0;
		envoyer_requete(requete);
	}
}

/* Fonction retournant texte du champs adresse du serveur de l'interface graphique */
char *lecture_addr_serveur(void)
{
	GtkWidget *entry_addr_srv;

	entry_addr_srv = (GtkWidget *) gtk_builder_get_object(p_builder, "entry_adr");

	return (char *)gtk_entry_get_text(GTK_ENTRY(entry_addr_srv));
}

/* Fonction retournant texte du champs port du serveur de l'interface graphique */
char *lecture_port_serveur(void)
{
	GtkWidget *entry_port_srv;

	entry_port_srv = (GtkWidget *) gtk_builder_get_object(p_builder, "entry_port");

	return (char *)gtk_entry_get_text(GTK_ENTRY(entry_port_srv));
}

/* Fonction retournant texte du champs login de l'interface graphique */
char *lecture_login(void)
{
	GtkWidget *entry_login;

	entry_login = (GtkWidget *) gtk_builder_get_object(p_builder, "entry_login");

	return (char *)gtk_entry_get_text(GTK_ENTRY(entry_login));
}

/* Fonction retournant texte du champs adresse du cadre Joueurs de l'interface graphique */
char *lecture_addr_adversaire(void)
{
	GtkWidget *entry_addr_j2;

	entry_addr_j2 = (GtkWidget *) gtk_builder_get_object(p_builder, "entry_addr_j2");

	return (char *)gtk_entry_get_text(GTK_ENTRY(entry_addr_j2));
}

/* Fonction retournant texte du champs port du cadre Joueurs de l'interface graphique */
char *lecture_port_adversaire(void)
{
	GtkWidget *entry_port_j2;

	entry_port_j2 = (GtkWidget *) gtk_builder_get_object(p_builder, "entry_port_j2");

	return (char *)gtk_entry_get_text(GTK_ENTRY(entry_port_j2));
}

/* Fonction affichant boite de dialogue si partie gagnee */
void affiche_fenetre_gagne(void)
{
	GtkWidget *dialog;

	GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;

	dialog = gtk_message_dialog_new(GTK_WINDOW(gtk_builder_get_object(p_builder, "window1")), flags, GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, "Fin de la partie.\n\n Vous avez gagné!!!", NULL);
	gtk_dialog_run(GTK_DIALOG (dialog));

	gtk_widget_destroy(dialog);
}

/* Fonction affichant boite de dialogue si partie perdue */
void affiche_fenetre_perdu(void)
{
	GtkWidget *dialog;

	GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;

	dialog = gtk_message_dialog_new(GTK_WINDOW(gtk_builder_get_object(p_builder, "window1")), flags, GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, "Fin de la partie.\n\n Vous avez perdu!", NULL);
	gtk_dialog_run(GTK_DIALOG (dialog));

	gtk_widget_destroy(dialog);
}

/* Fonction appelee lors du clique du bouton Se connecter */
static void clique_connect_serveur(GtkWidget *b)
{
	// TODO

}

/* Fonction desactivant bouton demarrer partie */
void disable_button_statr(void)
{
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object (p_builder, "button_start"), FALSE);
}

// Ajout

/* Fonction traitement signal bouton Demarrer partie */
static void clique_connect_adversaire(GtkWidget *b)
{
	if(newsockfd==-1)
	{
		// Deactivation bouton demarrer partie
		gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object (p_builder, "button_start"), FALSE);

		// Recuperation  adresse et port adversaire au format chaines caracteres
		addr_j2=lecture_addr_adversaire();
		port_j2=lecture_port_adversaire();

		printf("[Port joueur %d] Adresse j2 lue : %s\n",port, addr_j2);
		printf("[Port joueur %d] Port j2 lu : %s\n", port, port_j2);


		pthread_kill(thr_id, SIGUSR1);
	}
}

// Fin ajout

/* Fonction desactivant les cases du damier */
void gele_damier(void)
{
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxA1"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxB1"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxC1"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxD1"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxE1"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxF1"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxG1"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxH1"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxA2"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxB2"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxC2"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxD2"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxE2"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxF2"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxG2"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxH2"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxA3"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxB3"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxC3"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxD3"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxE3"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxF3"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxG3"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxH3"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxA4"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxB4"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxC4"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxD4"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxE4"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxF4"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxG4"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxH4"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxA5"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxB5"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxC5"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxD5"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxE5"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxF5"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxG5"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxH5"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxA6"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxB6"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxC6"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxD6"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxE6"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxF6"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxG6"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxH6"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxA7"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxB7"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxC7"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxD7"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxE7"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxF7"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxG7"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxH7"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxA8"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxB8"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxC8"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxD8"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxE8"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxF8"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxG8"), FALSE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxH8"), FALSE);
}

/* Fonction activant les cases du damier */
void degele_damier(void)
{
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxA1"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxB1"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxC1"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxD1"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxE1"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxF1"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxG1"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxH1"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxA2"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxB2"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxC2"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxD2"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxE2"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxF2"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxG2"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxH2"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxA3"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxB3"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxC3"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxD3"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxE3"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxF3"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxG3"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxH3"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxA4"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxB4"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxC4"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxD4"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxE4"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxF4"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxG4"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxH4"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxA5"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxB5"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxC5"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxD5"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxE5"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxF5"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxG5"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxH5"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxA6"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxB6"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxC6"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxD6"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxE6"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxF6"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxG6"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxH6"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxA7"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxB7"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxC7"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxD7"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxE7"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxF7"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxG7"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxH7"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxA8"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxB8"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxC8"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxD8"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxE8"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxF8"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxG8"), TRUE);
	gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object(p_builder, "eventboxH8"), TRUE);
}

/* Fonction permettant d'initialiser le plateau de jeu */
void init_interface_jeu(void)
{
	// XXX Initialiser damier
	set_case(3, 3, 1);
	set_case(4, 3, 0);
	set_case(3, 4, 0);
	set_case(4, 4, 1);

	// Initialisation des scores et des joueurs
	if(couleur==1)
	{
		set_label_J1("Vous");
		set_label_J2("Adversaire");
	}
	else
	{
		set_label_J1("Adversaire");
		set_label_J2("Vous");
	}

	set_score_J1(2);
	set_score_J2(2);
}

/* Fonction reinitialisant la liste des joueurs sur l'interface graphique */
void reset_liste_joueurs(void)
{
	GtkTextIter start, end;

	gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(gtk_text_view_get_buffer(GTK_TEXT_VIEW(gtk_builder_get_object(p_builder, "textview_joueurs")))), &start);
	gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(gtk_text_view_get_buffer(GTK_TEXT_VIEW(gtk_builder_get_object(p_builder, "textview_joueurs")))), &end);

	gtk_text_buffer_delete(GTK_TEXT_BUFFER(gtk_text_view_get_buffer(GTK_TEXT_VIEW(gtk_builder_get_object(p_builder, "textview_joueurs")))), &start, &end);
}

/* Fonction permettant d'ajouter un joueur dans la liste des joueurs sur l'interface graphique */
void affich_joueur(char *login, char *adresse, char *port)
{
	const gchar *joueur;

	joueur=g_strconcat(login, " - ", adresse, " : ", port, "\n", NULL);

	gtk_text_buffer_insert_at_cursor(GTK_TEXT_BUFFER(gtk_text_view_get_buffer(GTK_TEXT_VIEW(gtk_builder_get_object(p_builder, "textview_joueurs")))), joueur, strlen(joueur));
}


// Ajout

/* Fonction exécutée par le thread gérant les communications à travers la socket */
static void * f_com_socket(void *p_arg)
{
	int i, nbytes, col, lig;

	char buf[MAXDATASIZE], *tmp, *p_parse;
	int len, bytes_sent, t_msg_recu;

	sigset_t signal_mask;
	int fd_signal, rv;

	uint16_t type_msg, col_j2;
	uint16_t ucol, ulig;

	/* Association descripteur au signal SIGUSR1 */
	sigemptyset(&signal_mask);
	sigaddset(&signal_mask, SIGUSR1);

	if(sigprocmask(SIG_BLOCK, &signal_mask, NULL) == -1)
	{
		printf("[Pourt joueur %d] Erreur sigprocmask\n", port);

		return 0;
	}

	fd_signal = signalfd(-1, &signal_mask, 0);

	if(fd_signal == -1)
	{
		printf("[port joueur %d] Erreur signalfd\n", port);

		return 0;
	}

	/* Ajout descripteur du signal dans ensemble de descripteur utilisé avec fonction select */
	FD_SET(fd_signal, &master);

	if(fd_signal>fdmax)
	{
		fdmax=fd_signal;
	}


	while(1)
	{
		read_fds=master;	// copie des ensembles

		if(select(fdmax+1, &read_fds, &write_fds, NULL, NULL)==-1)
		{
			perror("Problème avec select");
			exit(4);
		}

		printf("[Port joueur %d] Entree dans boucle for\n", port);
		for(i=0; i<=fdmax; i++)
		{
			printf("[Port joueur %d] newsockfd=%d, iteration %d boucle for\n", port, newsockfd, i);

			if(FD_ISSET(i, &read_fds))
			{
				if(i==fd_signal)
				{
					/* Cas où de l'envoie du signal par l'interface graphique pour connexion au joueur adverse */

					if(newsockfd==-1)
					{
						memset(&hints, 0, sizeof(hints));
						hints.ai_family = AF_UNSPEC;
						hints.ai_socktype = SOCK_STREAM;
						rv = getaddrinfo(addr_j2, port_j2, &hints, &servinfo);

						if (rv != 0)
						{
							fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
						}

						// Création  socket  et  attachement
						for(p = servinfo; p != NULL; p = p->ai_next)
						{
							if((newsockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
							{
								perror("client: socket");
								continue;
							}
							if((connect(newsockfd, p->ai_addr, p->ai_addrlen)) == -1)
							{
								close(newsockfd);
								perror("client: connect");
								continue;
							}
							break;
						}

						if (p == NULL)
						{
							fprintf(stderr, "server: failed to bind\n");
							return 1;
						}

						freeaddrinfo(servinfo); // Libère structure

						FD_SET(newsockfd, &master);

						if(newsockfd>fdmax)
						{
							fdmax=newsockfd;
						}

						close(sockfd);
						FD_CLR(sockfd,&master);

						// Fermeture fermeture et suppression de fd_signal de l'ensemble master
						close(fd_signal);
						FD_CLR(fd_signal,&master);


						// XXX Debut de partie
						srand(time(NULL));
						couleur = rand() % 2;

						if (couleur == 0) {
							degele_damier();
						} else {
							gele_damier();
						}

						requete requete;
						requete.type = GAME_INITIATED;
						requete.couleur = couleur;
						requete.col = 0;
						requete.lig = 0;
						envoyer_requete(requete);

						// Initialisation interface graphique
						init_interface_jeu();
					}

				}

				if(i==sockfd)
				{ // Acceptation connexion adversaire


					if(newsockfd==-1)
					{
						addr_size=sizeof(their_addr);
						newsockfd=accept(sockfd, their_addr, (socklen_t *)&addr_size);

						if(newsockfd==-1)
						{
							perror("Problème avec select");
						}
						else
						{
							// Ajout du nouveau descripteur socket
							FD_SET(newsockfd, &master);

							if(newsockfd>fdmax)
							{
								fdmax=newsockfd;
							}

							// Fermeture socket ecoute car connexion deja etablie avec adversaire
							printf("[Port joueur %d] Bloc if du serveur, fermeture socket ecoute\n", port);

							FD_CLR(sockfd,&master);
							close(sockfd);

						}

						gtk_widget_set_sensitive((GtkWidget *) gtk_builder_get_object (p_builder, "button_start"), FALSE);
					}
				}
				else
				{ // Reception et traitement des messages du joueur adverse

					// XXX Reception requete
					if (newsockfd != -1) {
						requete requete = recevoir_requete();

						switch (requete.type) {
						case GAME_INITIATED:
							couleur = abs(requete.couleur - 1);

							if (couleur == 0) {
								degele_damier();
							} else {
								gele_damier();
							}

							init_interface_jeu();
							break;

						case TOKEN_PLAYED:
							jouer_token(requete.col, requete.lig, abs(couleur - 1), 1);
							update_score();
							if (!coup_possible()) {
								requete.type = GAME_END;
								requete.col = 0;
								requete.lig = 0;
								requete.couleur = 0;
								envoyer_requete(requete);

								if (couleur == 0) {
									if (get_score_J2() > get_score_J1()) {
										affiche_fenetre_gagne();
									} else if (get_score_J2() < get_score_J1()) {
										affiche_fenetre_perdu();
									} else {
										// draw
										affiche_fenetre_gagne();
									}
								} else if (couleur == 1) {
									if (get_score_J2() > get_score_J1()) {
										affiche_fenetre_perdu();
									} else if (get_score_J2() < get_score_J1()) {
										affiche_fenetre_gagne();
									} else {
										// draw
										affiche_fenetre_gagne();
									}
								}

								break;
							}
							degele_damier();
							break;

						case GAME_END:
							if (couleur == 0) {
								if (get_score_J2() > get_score_J1()) {
									affiche_fenetre_gagne();
								} else if (get_score_J2() < get_score_J1()) {
									affiche_fenetre_perdu();
								} else {
									// draw
									affiche_fenetre_gagne();
								}
							} else if (couleur == 1) {
								if (get_score_J2() > get_score_J1()) {
									affiche_fenetre_perdu();
								} else if (get_score_J2() < get_score_J1()) {
									affiche_fenetre_gagne();
								} else {
									// draw
									affiche_fenetre_gagne();
								}
							}

							break;
						}
					}
				}
			}
		}
	}

	return NULL;
}

// Fin ajout




int main (int argc, char ** argv)
{
	int i, j, ret;

	if(argc!=2)
	{
		printf("\nPrototype : ./othello num_port\n\n");

		exit(1);
	}


	/* Initialisation de GTK+ */
	gtk_init (& argc, & argv);

	/* Creation d'un nouveau GtkBuilder */
	p_builder = gtk_builder_new();

	if (p_builder != NULL)
	{
		/* Chargement du XML dans p_builder */
		gtk_builder_add_from_file (p_builder, "UI_Glade/Othello.glade", & p_err);

		if (p_err == NULL)
		{
			/* Recuparation d'un pointeur sur la fenetre. */
			GtkWidget * p_win = (GtkWidget *) gtk_builder_get_object (p_builder, "window1");


			/* Gestion evenement clic pour chacune des cases du damier */
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxA1"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxB1"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxC1"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxD1"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxE1"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxF1"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxG1"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxH1"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxA2"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxB2"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxC2"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxD2"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxE2"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxF2"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxG2"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxH2"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxA3"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxB3"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxC3"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxD3"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxE3"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxF3"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxG3"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxH3"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxA4"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxB4"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxC4"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxD4"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxE4"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxF4"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxG4"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxH4"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxA5"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxB5"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxC5"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxD5"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxE5"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxF5"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxG5"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxH5"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxA6"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxB6"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxC6"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxD6"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxE6"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxF6"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxG6"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxH6"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxA7"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxB7"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxC7"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxD7"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxE7"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxF7"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxG7"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxH7"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxA8"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxB8"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxC8"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxD8"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxE8"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxF8"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxG8"), "button_press_event", G_CALLBACK(coup_joueur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "eventboxH8"), "button_press_event", G_CALLBACK(coup_joueur), NULL);

			/* Gestion clic boutons interface */
			g_signal_connect(gtk_builder_get_object(p_builder, "button_connect"), "clicked", G_CALLBACK(clique_connect_serveur), NULL);
			g_signal_connect(gtk_builder_get_object(p_builder, "button_start"), "clicked", G_CALLBACK(clique_connect_adversaire), NULL);

			/* Gestion clic bouton fermeture fenetre */
			g_signal_connect_swapped(G_OBJECT(p_win), "destroy", G_CALLBACK(gtk_main_quit), NULL);



			/* Recuperation numero port donne en parametre */
			port=atoi(argv[1]);

			/* Initialisation du damier de jeu */
			for(i=0; i<8; i++)
			{
				for(j=0; j<8; j++)
				{
					damier[i][j]=-1;
				}
			}

			// Initialisation socket et autres objets, et création thread pour communications avec joueur adverse

			/* Initialisation socket communication */

			memset(&hints, 0, sizeof(hints));
			hints.ai_family = AF_UNSPEC;
			hints.ai_socktype = SOCK_STREAM;
			hints.ai_flags = AI_PASSIVE;

			status = getaddrinfo(NULL, argv[1], &hints, &servinfo);

			if(status!=0)
			{
				fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
				exit(1);
			}

			for(p = servinfo; p != NULL; p = p->ai_next)
			{
				if((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
				{
					perror("server: socket");
					continue;
				}
				if(bind(sockfd, p->ai_addr, p->ai_addrlen) == -1)
				{
					close(sockfd);
					perror("server: bind");
					continue;
				}
				printf("[Port joueur %s] Ok socket correct\n", argv[1]);
				break;
			}

			if(p == NULL)
			{
				fprintf(stderr, "server: failed to bind\n");
				exit(2);
			}

			// Libérer la liste pointeur sur structures addrinfo des que plus besoin
			freeaddrinfo(servinfo);

			listen(sockfd, 1);

			FD_ZERO(&master);
			FD_ZERO(&read_fds);

			FD_SET(sockfd, &master);
			read_fds=master;	// copie des ensembles
			fdmax=sockfd;


			/* Creation thread serveur ecoute message */
			ret=pthread_create(&thr_id, NULL, f_com_socket, NULL);

			if(ret!=0)
			{
				fprintf(stderr, "%s", strerror(ret));

				exit(1);
			}



			gtk_widget_show_all(p_win);
			gtk_main();
		}
		else
		{
			/* Affichage du message d'erreur de GTK+ */
			g_error ("%s", p_err->message);
			g_error_free (p_err);
		}
	}


	return EXIT_SUCCESS;
}
